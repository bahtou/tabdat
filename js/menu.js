(function(window) {

  'use strict';

  /**
   * Extend Object helper function.
   */
  function extend(a, b) {
    for (var key in b) {
      if (b.hasOwnProperty(key)) {
        a[key] = b[key];
      }
    }
    return a;
  }

  /**
   * Each helper function.
   */
  function each(collection, callback) {
    for (var i = 0; i < collection.length; i++) {
      var item = collection[i];
      callback(item);
    }
  }

  /**
   * Menu Constructor.
   */
  function Menu(options) {
    this.options = extend({}, this.options);
    extend(this.options, options);
    this._init();
  }

  /**
   * Menu Options.
   */
  Menu.prototype.options = {
    type: 'slide-bottom', // The menu type
    menuOpenerClass: 'tabdat__link' // The menu opener class names (i.e. the buttons)
  };

  /**
   * Initialise Menu.
   */
  Menu.prototype._init = function() {
    this.menu = document.getElementsByClassName('tabdat')[0];
  };

  /**
   * Open Menu.
   */
  Menu.prototype.open = function(e) {
    document.getElementsByClassName('tabdat__link--active')[0].classList.remove('tabdat__link--active');
    e.target.parentElement.classList.add('tabdat__link--active');
    this.menu.classList.add('tabdat--active');
  };

  /**
   * Close Menu.
   */
  Menu.prototype.close = function(e) {
    document.getElementsByClassName('tabdat__link--active')[0].classList.remove('tabdat__link--active');
    e.target.parentElement.classList.add('tabdat__link--active');
    this.menu.classList.remove('tabdat--active');
  };

  /**
   * Add to global namespace.
   */
  window.Menu = Menu;

})(window);
